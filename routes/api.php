<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/
Route::prefix('auth')->group(function () {
    Route::post('register', 'AuthController@register');
    Route::post('login', 'AuthController@login');
    Route::get('refresh', 'AuthController@refresh');

    Route::group(['middleware' => 'auth:api'], function(){
        Route::get('user', 'AuthController@user');
        Route::get('logout', 'AuthController@logout');
    });
});

Route::middleware('jwt.auth')->group(function(){

    Route::get('logout', 'AuthController@logout');
});

Route::group(['middleware' => 'auth:api'], function(){
    // Users
    Route::get('users', 'UserController@index')->middleware('isAdmin');
    Route::get('users/{id}', 'UserController@show')->middleware('isAdminOrSelf');
});


// liste les questions
Route::get('questions','QuestionController@index');
// récupère une question
Route::get('question/{id}','QuestionController@show');
// création d'une question
Route::post('question','QuestionController@store');
// maj d'une question
Route::put('question','QuestionController@store');
// suppression d'une question
Route::delete('question/{id}','QuestionController@destroy');

// Questions Reponses
// liste les questions avec les propositions associées
Route::get('questionreponses','QuestionReponsesController@index');
// récupère une  questions avec les propositions associées
Route::get('questionreponses/{id}','QuestionReponsesController@show');
// création d'une  questionsavec les propositions associées
Route::post('questionreponses','QuestionReponsesController@store');
// maj d'une  question avec les propositions associées
Route::put('questionreponses','QuestionReponsesController@store');
// suppression d'une  question avec les propositions associées
Route::delete('questionreponses/{id}','QuestionReponsesController@destroy');

// tags
Route::get('tags','TagController@index');
Route::get('tags/{id}','TagController@show');
Route::post('tags','TagController@store');
Route::put('tags','TagController@store');
Route::delete('tags/{id}','TagController@destroy');

