<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class QuestionReponses extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);
        return [
            'id' => $this->id,
            'id_question' => $this->id_question,
            'id_proposition' => $this->id_proposition,
            'libProp' => $this->libelle,
            'reponse' => $this->reponse,
            'nbPointsReponse' => $this->points
        ];

    }


    public function with($request){
        return [
            'version' => '1.0.0'
        ];
    }
}
