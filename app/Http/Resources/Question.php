<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        // return parent::toArray($request);


        return [
            'id'            => $this->id,
            'id_user'       => $this->id_user,
            'id_categorie'  => $this->id_categorie,
            'libelle'       => $this->libelle,
            'points'        => $this->points,
            'imageNom'      => $this->imageNom
        ];

    }


    public function with($request){
        return [
            'version'   =>  '1.0.0'
        ] ;
    }

}
