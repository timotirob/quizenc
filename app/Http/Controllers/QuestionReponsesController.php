<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\QuestionReponses;
use App\Http\Resources\QuestionReponses as QuestionReponsesResource;

class QuestionReponsesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questionreponses = QuestionReponses::paginate(15);

        return QuestionReponsesResource::collection($questionreponses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $questionreponses =
            $request->isMethod('put') ?
                QuestionReponses::findOrFail($request->id_questionreponses) :
                new QuestionReponses ;
        $questionreponses->id = $request->input('id_questionreponses');
        $questionreponses->id_proposition = $request->input('id_proposition');
        $questionreponses->id_question = $request->input('id_question');
        $questionreponses->libelle = $request->input('libelle');
        $questionreponses->reponse = $request->input('reponse');
        $questionreponses->points = $request->input('points');

        if ( $questionreponses->save()) {
            return new QuestionReponsesResource($questionreponses);
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $questionreponses = QuestionReponses::findOrFail($id);
        return new QuestionReponsesResource($questionreponses);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $questionreponses = QuestionReponses::findOrFail($id);

        if ($questionreponses->delete()){
            return new QuestionReponsesResource($questionreponses);
        }
    }
}
