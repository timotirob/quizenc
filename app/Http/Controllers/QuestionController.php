<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Http\Resources\Question as QuestionRessource;

class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $questions = Question::paginate(10);
        return QuestionRessource::collection($questions);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $question = $request->isMethod('put') ? Question::findOrFail($request->id_question) : new Question ;


        $question->id = $request->input('id_question');
        $question->id_user = $request->input('id_user');
        $question->id_categorie = $request->input('id_categorie');
        $question->libelle = $request->input('libelle');
        $question->points = $request->input('points');


        if ($request->filled('imageNom')) {

            $question->imageNom = "/images/".$request->input('imageNom');
            // $question->contenuImage = $request->input('contenuImage');

            $request->contenuImage->move(public_path('images'), $question->imageNom);
        }
        else $question->imageNom = "/images/blank.jpg";







        if ($question -> save()) {
            return new QuestionRessource($question) ;
        }



        /** OK  */
        //return response()->json([$request->libelle]) ;

        //return response()->json([$request->all()]);


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $question = Question::findOrFail($id) ;
        return new QuestionRessource($question);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = Question::findOrFail($id);

        if ($question->delete()){
            return new QuestionRessource($question);
        }
    }
}
