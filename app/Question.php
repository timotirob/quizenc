<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //



    protected static function boot() {
        parent::boot() ;
        static::deleted(function ($question) {
            $question->questionsreponses()->delete();
            $question->tags()->delete() ;

        });
        }




    public function questionsreponses(){
        return $this->hasMany('App\QuestionReponses','id_question') ;
    }

    public function tags(){
        return $this->hasMany('App\Tag','id_question') ;
    }

}
