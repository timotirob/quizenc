<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionReponsesTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question_reponses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('id_proposition');
            $table->bigInteger('id_question');
            $table->text('libelle');
            $table->boolean('reponse');
            $table->float('points');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question_reponses');
    }
}
