<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(App\Question::class, function (Faker $faker) {
    return [
        //
        'libelle' => $faker->text(250),
        'points' => $faker->randomDigit,
        'id_user' => 1,
        'id_categorie' => 1
    ];
});
