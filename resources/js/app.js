/**
 * First, we will load all of this project's Javascript utilities and other
 * dependencies. Then, we will be ready to develop a robust and powerful
 * application frontend using useful Laravel and JavaScript libraries.
 */

import('./bootstrap');
import 'es6-promise/auto';
import axios from 'axios'
import VueAuth from '@websanova/vue-auth'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'

import auth from './auth'


import '@mdi/font/css/materialdesignicons.css'
import 'vuetify/dist/vuetify.min.css'
import Vue from 'vue';
import Vuetify from 'vuetify';
import Vuelidate from 'vuelidate'
import Vuex from 'vuex'

import router from '@/js/routes.js' ;

import Routes from '@/js/routes.js';

import App from '@/js/views/App';

Vue.use(Vuetify)

Vue.use(Vuelidate);
Vue.use(Vuex);

Vue.router = router
Vue.use(VueRouter)
Vue.use(VueAxios, axios)
axios.defaults.baseURL = `${process.env.MIX_APP_URL}/api`
Vue.use(VueAuth, auth)

const app = new Vue({
    el: '#app',
    router: Routes,
    render: h =>h(App),
});

export default app;

