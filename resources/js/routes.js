import Vue from 'vue';
import VueRouter from 'vue-router';

import Accueil from '@/js/components/Accueil';
import Apropos from '@/js/components/Apropos';
import Ajout from '@/js/components/Ajout';
import Liste from '@/js/components/Liste';
import Register from '@/js/components/Register';
import Login from '@/js/components/Login';
import Logout from '@/js/components/Logout';



Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'accueil',
            component: Accueil,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/register',
            name: 'register',
            component: Register,
            meta: {
                auth: false
            }
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
            meta: {
                auth: false
            }
        },
        {
            path: '/logout',
            name: 'logout',
            component: Logout,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/apropos',
            name: 'apropos',
            component: Apropos,
            meta: {
                auth: undefined
            }
        },
        {
            path: '/ajout',
            name: 'ajout',
            component: Ajout,
            meta: {
                auth: true
            }
        },
        {
            path: '/liste',
            name: 'liste',
            component: Liste,
            meta: {
                auth: true
            }
        }
    ]
});

export default router;
